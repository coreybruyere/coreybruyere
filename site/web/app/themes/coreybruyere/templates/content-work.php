
<?php $feat_img_lg    = get_field('full_display_featured_image'); ?>
<?php $feat_img_md    = get_field('medium_display_featured_image'); ?>
<?php $feat_img_sm    = get_field('small_display_featured_image'); ?>
<?php $feat_desc      = get_field('featured_item_description'); ?>
<?php $feat_site      = get_field('site_link_url'); ?>
<?php $feat_year      = get_field('year_completed'); ?>
<?php $feat_team      = have_rows('site_team'); ?>
<?php $feat_title     = get_the_title(); ?>
<?php $feat_arch_link = get_post_type_archive_link('work') . '#' . sanitize_title_with_dashes($feat_title); ?>

<article class="feature feature--decor" id="<?php echo sanitize_title_with_dashes($feat_title); ?>">
  <div class="l-container l-container--wide">
    <div class="l-block u-clearfix">
      <div class="u-float-left">
        <?php if ($feat_year) : ?>
          <div class="u-h6 u-margin-b-none u-muted"><?php echo $feat_year; ?></div>
        <?php endif; ?>

        <h3 class="feature__title">
          <?php the_title(); ?>
        </h3>

        <?php if ($feat_site) : ?>
          <a class="btn btn--small btn--secondary" href="<?php echo $feat_site; ?>"><?php _e('View Site', 'sage'); ?></a>
        <?php endif; ?>
      </div>
    </div>
    <div class="display">
      <?php if ($feat_img_lg) : ?>
      <div class="display__item display__item--lg">
        <div class="display__view">
          <img srcset="<?php echo $feat_img_lg['sizes']['featured-full-3x']; ?> 3x, <?php echo $feat_img_lg['sizes']['featured-full-2x']; ?> 2x" sizes="13vw" alt="<?php echo $feat_img_lg['alt']; ?>">
        </div>
      </div>
      <?php endif; ?>

      <?php if ($feat_img_md) : ?>
      <div class="display__item display__item--md">
        <div class="display__view display__view--pull">
          <img srcset="<?php echo $feat_img_md['sizes']['featured-full-3x']; ?> 3x, <?php echo $feat_img_md['sizes']['featured-full-2x']; ?> 2x" sizes="23vw" alt="<?php echo $feat_img_md['alt']; ?>">
        </div>
      </div>
      <?php endif; ?>

      <?php if ($feat_img_sm) : ?>
      <div class="display__item display__item--sm">
        <div class="display__view display__view--pull">
          <img srcset="<?php echo $feat_img_sm['sizes']['featured-full-3x']; ?> 3x, <?php echo $feat_img_sm['sizes']['featured-full-2x']; ?> 2x" sizes="(min-width: 34.375em) 64vw, 100vw" alt="<?php echo $feat_img_sm['alt']; ?>">
        </div>
      </div>
      <?php endif; ?>
    </div>
  </div><!-- /.l-container -->
</article><!-- /.feature -->
