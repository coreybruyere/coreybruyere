<footer class="site-footer l-container l-container--wide" role="contentinfo">
  <div class="l-flag l-flag--bottom l-block">
    <div class="l-flag__image">
      <a class="branding branding--inherit u-shim-all-none" href="<?= esc_url(home_url('/')); ?>" aria-label="<?php _e('Home Page', 'sage'); ?>">
        <svg class="branding__logo  icon">
          <use xlink:href="#icon-logo-small"></use>
        </svg><!-- /.branding__logo -->
      </a><!-- /.branding -->
    </div><!-- /.l-flag__image -->
    <div class="l-flag__body">
      <div class="site-footer__wrap">
        <div class="u-shim-l-lg">
          <small>&copy; <?php echo date('Y'); ?> <?php echo get_bloginfo(); ?></small>
        </div>
        <div>
          <ul class="l-inline-list l-blank-list">
            <?php
              $groupID='91';
              $custom_field_keys = get_post_custom_keys($groupID);
            ?>
            <?php foreach ( $custom_field_keys as $key => $fieldkey ) : ?>
              <?php if (stristr($fieldkey,'field_')) : ?>
                <?php
                  $field      = get_field_object($fieldkey, $groupID);
                  $field_name = $field['name'];
                  $field_val  = get_field($field_name, 'option');
                ?>
                <li class="u-shim-l-lg">
                  <a class="alt-link" href="<?php echo $field_val; ?>" aria-label="<?php _e($field_name, 'sage'); ?>">
                    <svg class="icon icon--md">
                      <use xlink:href="#icon-<?php echo $field_name; ?>"></use>
                    </svg>
                  </a>
                </li>
              <?php endif; ?>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </div><!-- /.l-flag__body -->
  </div><!-- /.l-flag -->
  <?php dynamic_sidebar('sidebar-footer'); ?>
  <small>Built with <a href="https://roots.io/">Roots</a> and hosted on <a href="https://www.digitalocean.com/">Digital Ocean</a></small>
</footer><!-- /.site-footer -->
