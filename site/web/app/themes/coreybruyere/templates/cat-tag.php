<span class="u-hide-visually"><?php _e('Category', 'sage'); ?></span>
<ul class="l-blank-list">
  <?php $categories = get_the_category(); ?>
  <?php foreach($categories as $category): ?>
    <li class="cat-tag cat-tag--<?php echo $category->category_nicename; ?>">
      <a class="cat-tag__link" href="<?php echo get_category_link( $category->term_id ); ?>"><?php echo $category->name; ?></a>
    </li>
  <?php endforeach; ?>
</ul>
