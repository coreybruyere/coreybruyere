<?php $work_link = get_post_type_archive_link('work') . '#' . sanitize_title_with_dashes(get_the_title()); ?>
<div class="post">
  <div class="l-media callout u-margin-all-none">
    <?php $icon = ( 'post' == get_post_type() ? 'pencil2' : 'display' ); ?>
    <span class="u-hide-visually"><?php ('post' == get_post_type() ? _e('Blog Post', 'sage') : _e('Portfolio Item Post', 'sage')); ?></span>
    <svg class="l-media__left callout__icon callout__icon--sm icon" aria-hidden="true">
      <use xlink:href="#icon-<?php echo $icon?>"></use>
    </svg>

    <div class="l-media__body">
      <div class="post__heading">
        <h3 class="post__title u-margin-all-none"><a href="<?php echo ( 'work' == get_post_type() ? $work_link : the_permalink() ); ?>"><?php the_title(); ?></a></h3>
        <div>
          <!-- <?php
            // Category Tags
            get_template_part('templates/entry-meta', get_post_type() != 'post' ? get_post_type() : get_post_format());
          ?> -->
          <?php
            // Category Tags
            get_template_part('templates/cat-tag', get_post_type() != 'post' ? get_post_type() : get_post_format());
          ?>
        </div>
      </div><!-- /.post__heading -->
    </div><!-- /.l-media__body -->
  </div><!-- /.l-media /.callout -->
</div><!-- /.post -->
