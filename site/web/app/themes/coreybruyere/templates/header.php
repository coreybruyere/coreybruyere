<header class="site-header l-container l-container--wide" role="banner">
  <a class="branding" href="<?= esc_url(home_url('/')); ?>" aria-label="<?php _e('Home Page', 'sage'); ?>">
    <svg class="branding__logo  icon">
      <use xlink:href="#icon-logo-small"></use>
    </svg>
  </a>
  <nav class="nav" role="navigation">
    <?php
    if (has_nav_menu('primary_navigation')) :
      wp_nav_menu([
        'theme_location' => 'primary_navigation',
        'menu_class'     => 'l-blank-list'
      ]);
    endif;
    ?>
  </nav>
  <!-- fallback to mailto href if js is disabled -->
  <a href="mailto:hello@coreybruyere.com" class="btn btn--ghost tooltip js-graceful-toggle js-tooltip js-copy shame-primary-btn" id="js-graceful-btn" data-tooltip="Email was copied to clipboard" data-placement="bottom bottom-right" data-trigger="click" data-toggle="js-graceful-btn" data-copy="<?php _e('hello@coreybruyere.com', 'sage'); ?>" aria-label="<?php _e('Contact me at: hello@coreybruyere.com', 'sage'); ?>"><?php _e('Contact', 'sage'); ?></a>
  <!-- hacky fallback link for Safari aka the new IE -->
  <a href="mailto:hello@coreybruyere.com" class="btn btn--ghost u-hide shame-safari-btn"><?php _e('Contact', 'sage'); ?></a>
</header>
</div><!-- /.site-header -->
