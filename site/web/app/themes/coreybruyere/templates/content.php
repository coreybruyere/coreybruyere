<article <?php post_class('post'); ?>>
  <header>
    <?php get_template_part('templates/entry-meta'); ?>
    <h3 class="post__title u-margin-b-none">
      <a class="post__link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
      <span class="post__status" aria-hidden="true"></span>
    </h3>
  </header>
  <!-- <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div> -->
</article>
