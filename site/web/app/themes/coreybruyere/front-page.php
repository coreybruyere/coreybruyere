

<section class="section">
  <?php
    // Hero Block
    get_template_part('templates/hero', get_post_type());
  ?>
</section><!-- /.section -->

  <?php
    // OLD - Front Page Promo Block
    // OLD - get_template_part('templates/front-promo');
  ?>

<section class="section">
  <?php
    // RELOCATE - Tabs
    // RELOCATE - get_template_part('templates/tabs');
  ?>

  <div class="l-block">
    <div class="l-container l-container--wide">
      <?php
        // Set up custom post WP_query
        $custom_query = new WP_Query(array(
          'post_type'      => array('work', 'post'),
          'posts_per_page' => 8
        ));
      ?>
      <?php while ($custom_query->have_posts()) : $custom_query->the_post(); ?>
        <?php get_template_part('templates/content-front'); ?>

      <?php endwhile; ?>

      <?php wp_reset_postdata(); ?>
    </div><!-- /.l-container -->
  </div><!-- /.l-block -->

  <div class="l-block">
    <div class="l-this-or-this">
      <div class="l-this-or-this__this">
        <a href="<?php echo get_post_type_archive_link('work'); ?>" class="btn btn--secondary"><?php _e('View All Work', 'sage'); ?></a>
      </div>
      <span class="l-this-or-this__or"><?php _e('Or', 'sage'); ?></span>
      <div class="l-this-or-this__this">
        <a href="<?php echo get_page_link( get_page_by_title('Articles')->ID ); ?>" class="btn btn--secondary"><?php _e('View Posts', 'sage'); ?></a>
      </div>
    </div><!-- /.l-this-or-this -->

  </div><!-- /.l-block -->
</section><!-- /.section -->
