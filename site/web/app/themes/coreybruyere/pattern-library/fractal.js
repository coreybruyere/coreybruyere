'use strict';

/*
 * Require the Fractal module
 */
const fractal = module.exports = require('@frctl/fractal').create();

/*
 * Give your project a title.
 */
fractal.set('project.title', 'Test');

/*
 * Tell Fractal where to look for components.
 */
fractal.components.set('path',  __dirname + '/src/patterns');

/*
 * Rename Components Label
 */
fractal.components.set('label', 'Patterns'); // default is 'Components'

/*
 * Tell Fractal where to look for documentation pages.
 */
fractal.docs.set('path', __dirname + '/src/docs');

/*
 * Tell the Fractal web preview plugin where to look for static assets.
 */
fractal.web.set('static.path',  __dirname + '/dist');

/*
 * Static HTML build location
 */
fractal.web.set('builder.dest', __dirname + '/build');


fractal.components.set('default.collated', true); // default is false

fractal.components.set('default.collator', function(markup, item) {
    return `<!-- Start: @${item.handle} -->\n${markup}\n<!-- End: @${item.handle} -->\n`
});
